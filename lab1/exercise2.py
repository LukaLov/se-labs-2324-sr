def centered_average(int_list):
    # Sort the list in ascending order
    sorted_list = sorted(int_list)
    
    # Remove the first and last elements (smallest and largest values)
    trimmed_list = sorted_list[1:-1]
    
    # Calculate the sum of the trimmed list
    total = sum(trimmed_list)
    
    # Calculate the centered average (integer division)
    average = total // len(trimmed_list)
    
    return average

def main():
    # Input comma-separated integers as a string and convert to a list of integers
    input_str = input("Enter a comma-separated list of integers: ")
    int_list = [int(x) for x in input_str.split(',')]

    # Check if the list has at least 3 elements
    if len(int_list) >= 3:
        # Call the function to calculate the centered average
        result = centered_average(int_list)
        print(f"Centered Average: {result}")
    else:
        print("The list must have at least 3 elements to calculate the centered average.")

if __name__ == "__main__":
    main()
