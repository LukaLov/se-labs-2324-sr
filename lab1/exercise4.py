def count_characters(input_string):
    # Initialize counters for uppercase, lowercase, and digits
    uppercase_count = 0
    lowercase_count = 0
    digit_count = 0

    # Iterate through the characters in the input string
    for char in input_string:
        if char.isupper():
            uppercase_count += 1
        elif char.islower():
            lowercase_count += 1
        elif char.isdigit():
            digit_count += 1

    # Return the counts as a tuple
    return (uppercase_count, lowercase_count, digit_count)

def main():
    # Input a string
    input_string = input("Enter a string: ")

    # Call the function to count characters
    result = count_characters(input_string)
    print("Uppercase Letters:", result[0])
    print("Lowercase Letters:", result[1])
    print("Numbers:", result[2])

if __name__ == "__main__":
    main()
