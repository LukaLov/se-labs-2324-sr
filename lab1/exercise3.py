def has_22(int_list):
    for i in range(len(int_list) - 1):
        if int_list[i] == 2 and int_list[i + 1] == 2:
            return True
    return False

def main():
    # Input comma-separated integers as a string and convert to a list of integers
    input_str = input("Enter a comma-separated list of integers: ")
    int_list = [int(x) for x in input_str.split(',')]

    # Call the function to check if the list contains a 2 next to a 2
    result = has_22(int_list)
    if result:
        print("The list contains a 2 next to a 2.")
    else:
        print("The list does not contain a 2 next to a 2.")

if __name__ == "__main__":
    main()
