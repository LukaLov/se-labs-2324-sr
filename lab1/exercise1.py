def count_even_integers(input_str):
    # Split the input string by commas and convert the parts to integers
    numbers = [int(x) for x in input_str.split(',')]

    # Function to check if a number is even
    def is_even(num):
        return num % 2 == 0

    # Use the is_even function to count even numbers
    even_count = sum(1 for num in numbers if is_even(num))

    return even_count

def main():
    # Input comma-separated integers as a string
    input_str = input("Enter a comma-separated list of integers: ")

    # Call the function to count even integers and print the result
    even_count = count_even_integers(input_str)
    print(f"Number of even integers: {even_count}")

if __name__ == "__main__":
    main()
